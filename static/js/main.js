function showError(container, errorMessage) {
      container.className = 'error';
      var msgElem = document.createElement('span');
      msgElem.className = "error-message";
      msgElem.innerHTML = errorMessage;
      container.appendChild(msgElem);
}

function resetError(container) {
      container.className = '';
      if (container.lastChild.className == "error-message") {
        container.removeChild(container.lastChild);
      }
}


function validate(e, form) {
      
      var elems = form.elements;
        
      resetError(elems.email.parentNode);
      resetError(elems.comment.parentNode);

      
      if (!elems.email.value) {
        showError(elems.email.parentNode, ' Please enter a valid email address');
	e.preventDefault();
      }
      else {
	email = elems.email.value;
	email_pattern = /[0-9a-z_]+@[0-9a-z_]+\.[a-z]{2,5}/i;
	res = email_pattern.test(email);
	if(res == false){
	  showError(elems.email.parentNode, ' An email have to be such like igor123@gmail.com');
	  e.preventDefault();
	}
      }
      

      if (!elems.comment.value) {
        showError(elems.comment.parentNode, ' Please enter some comment');
	e.preventDefault();
      } 
}


