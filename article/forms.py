from models import Comments, Tags
from django import forms
from django.core.exceptions import ValidationError


class CommentsForm(forms.ModelForm):
    comment = forms.CharField(widget=forms.Textarea)
    author = forms.CharField(max_length=70, required=False)
    email = forms.EmailField(required=False)
    website = forms.CharField(max_length=255, required=False)

    class Meta:
        model = Comments
        fields = ('comment', 'author', 'email', 'website')

