from django import template

register = template.Library()

@register.simple_tag
def render_avatar(author):
    author = author.lower()
    avatar = '<img src="/static/img/'+author+'.png" width="50px" />'
    return avatar
