from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from annoying.decorators import render_to

from models import Categories, Articles, Comments, Tags
from django.db.models import Count, Avg

from .forms import CommentsForm


# Create your views here.
@render_to('last_posts.html')
def main(request):
    categories = Categories.objects.all()
    articles = Articles.objects.filter().order_by('-id')[:6]. \
        annotate(Count('comments'), Avg('category'))
    return {'categories': categories, 'articles': articles}


@render_to('category.html')
def category(request, category_id):
    category = Categories.objects.get(id=category_id)
    categories = Categories.objects.all()
    articles = Articles.objects.filter(category_id=category_id). \
        order_by('-id').annotate(Count('comments'), Avg('category'))
    return {'categories': categories,
            'category': category,
            'articles': articles}


@render_to('article.html')
def article(request, article_id):
    if request.method == 'POST':
        form = CommentsForm(request.POST)
        if form.is_valid():
            textfield = request.POST.get('comment')
            usrname = request.POST.get('author')
            email = request.POST.get('email')
            website = request.POST.get('website')
            if request.user.is_authenticated():
                Comments.objects.create(
                       comment=textfield,
                       article_id=article_id,
                       author=request.user.username,
                       email=request.user.email,
                       website='www.localhost.com')
            else:
                if not usrname:
                    Comments.objects.create(
                           comment=textfield,
                           article_id=article_id,
                           author='Anonymous',
                           email=email,
                           website=website)
                else:
                    Comments.objects.create(
                           comment=textfield,
                           article_id=article_id,
                           author=usrname,
                           email=email,
                           website=website)
    else:
        form = CommentsForm()
    tags = Tags.objects.filter(article_id=article_id)
    article = Articles.objects.get(id=article_id)
    comments = Comments.objects.filter(article_id=article_id)
    categories = Categories.objects.all()
    return {'categories': categories, 'category': category,
            'article': article, 'comments': comments, 
            'form': form, 'tags': tags}


@render_to('posts_by_tag.html')
def tag(request, tag_name):
    categories = Categories.objects.all()
    article_id = Tags.objects.filter(name=tag_name)
    obj = [] 
    for a in article_id:
        obj.append(Articles.objects.filter(id=a.article_id).order_by('-id')[:6]. \
            annotate(Count('comments'), Avg('category')))
    return {'categories': categories, 'articles': obj}

