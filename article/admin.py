from django.contrib import admin

from models import Categories, Articles, Comments, Tags

# Register your models here.


class CategoriesAdmin(admin.ModelAdmin):
    list_display = ('name')

admin.site.register(Categories)


class ArticlesAdmin(admin.ModelAdmin):
    list_display = ('title', 'text', 'category', 'image')

admin.site.register(Articles)


class CommentsAdmin(admin.ModelAdmin):
    list_display = ('comment', 'author', 'email', 'website')

admin.site.register(Comments)


class TagsAdmin(admin.ModelAdmin):
    list_display = ('name')

admin.site.register(Tags)

