from __future__ import unicode_literals
from django.db import models

# Create your models here.


class Categories(models.Model):
    class Meta():
        db_table = 'Categories'
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Articles(models.Model):
    class Meta():
        db_table = 'Articles'
    title = models.CharField(max_length=255)
    text = models.TextField()
    category = models.ForeignKey(Categories)
    image = models.ImageField(upload_to='photos/', default='photos/no-img.jpg')

    def __str__(self):
        return self.title


class Comments(models.Model):
    class Meta():
        db_table = 'Comments'
    comment = models.TextField()
    author = models.CharField(max_length=70, blank=True)
    email = models.EmailField()
    website = models.CharField(max_length=255)
    article = models.ForeignKey(Articles)

    def __str__(self):
        return self.comment


class Tags(models.Model):
    class Meta():
        db_table = 'Tags'
    name = models.CharField(max_length=70)
    article = models.ForeignKey(Articles)

    def __str__(self):
        return self.name


