1. mkdir blog
2. cd blog
3. git clone git@bitbucket.org:degtyarenkoi/awesome-blog.git
4. virtualenv venv
5. source venv/bin/activate
6. cd awesome_blog
7. sudo apt-get install libjpeg8 libjpeg-dev libfreetype6 libfreetype6-dev zlib1g-dev
8. pip install -r req.txt
9. python manage.py migrate
10. python manage.py runserver
11. follow the http://localhost:8000/
12 . admin panel is available by http://localhost:8000/admin link with login 'admin' and password 'qwe123qwe'
13. 404 error is working on DEBUG = False mode
